SIMPIC - Simple 1D PIC prototype
================================

SIMPIC is extracted from [OOPD1]_ source code to demonstrate [StarPU]_
possibilities.



The code is a simple one-dimensional electrostatic code (meaning
:math:`\nabla\times\mathbf{B}=\partial\mathbf{B}/\partial t \approx 0`
so that :math:`\mathbf{E}=-\nabla\phi`), with self and applied
electric fields *E* directed along coordinate *x*. There are no
variations in *y* or *z* directions. The plane parallel problem
consists of single (electron) species.

Description of SIMPIC
~~~~~~~~~~~~~~~~~~~~~

Particle in cell (PIC) is a particle-mesh method. This means that the phase space distribution of particles :math:`f(v)`,
is represented by collocation of discrete computational particles, with each computational particle representing ``np2c``
real particles. Instead of computing the interactions between each particle, the particle density (and higher moments for some PIC methods) is interpolated (weighed) to the mesh. A field equation (Poisson’s equation) is solved using this information and the computed force is interpolated back to the particle which is used to update its velocity and position. This alternating field solve and particle push is repeated for each time step. For electrostatic PIC, Poisson’s equation is solved for potential and then numerically differentiated to determine the electric field.

Poisson solver
--------------

Poisson’s equation for electrostatics is applicable when the magnetic field is not present or not time varying
(magnetostatic). In this case, the electric field may be written as the gradient of a scalar potential field, :math:`\phi`:

.. math:: \mathbf{E}=-\nabla\phi 

Gauss’s law then becomes:

.. math:: \nabla\cdot(\epsilon\mathbf{E})=-\nabla\cdot(\epsilon\nabla\phi)=\rho

In this equation  :math:`\rho` is charge density, obtained from particle date (for a single species of particles, :math:`\rho=qn`,
where  :math:`q` is the charge per particle and  :math:`n` is the number density of the species).

For meshes with a uniform value of :math:`\epsilon`, the Laplacian operator may be used:

Cartesian:

.. math:: \nabla^2\phi=\frac{\mathrm{d^2}\phi}{\mathrm{d}x^2}

Cylindrical:

.. math:: \nabla^2\phi=\frac{1}{r}\frac{\mathrm{d}}{\mathrm{d}r}\left (r\frac{\mathrm{d}\phi}{\mathrm{d}r}\right ) = \frac{\mathrm{d^2}\phi}{\mathrm{d}r^2} + \frac{1}{r}\frac{\mathrm{d\phi}}{\mathrm{d}r}

Spherical:

.. math:: \nabla^2\phi=\frac{1}{r^2}\frac{\mathrm{d}}{\mathrm{d}r}\left (r^2\frac{\mathrm{d}\phi}{\mathrm{d}r}\right ) = \frac{1}{r}\frac{\mathrm{d^2}}{\mathrm{d}r^2}\left (r\phi \right ) = \frac{\mathrm{d^2}\phi}{\mathrm{d}r^2} + \frac{2}{r}\frac{\mathrm{d\phi}}{\mathrm{d}r}

In finite difference form for a uniform mesh:

Cartesian:

.. math:: \nabla^2\phi_i=\frac{\phi_{i-1}-2\phi_{i}+\phi_{i+1}}{\Delta x^2}+\mathcal{O}(\Delta x^2)

Cylindrical:

.. math:: \nabla^2\phi_i=\frac{\phi_{i-1}-2\phi_{i}+\phi_{i+1}}{\Delta r^2} + \frac{1}{r_i}\frac{\phi_{i+1}-\phi_{i-1}}{2\Delta r}+\mathcal{O}(\Delta r^2)

Spherical:

.. math:: \nabla^2\phi_i=\frac{\phi_{i-1}-2\phi_{i}+\phi_{i+1}}{\Delta r^2} + \frac{2}{r_i}\frac{\phi_{i+1}-\phi_{i-1}}{2\Delta r}+\mathcal{O}(\Delta r^2)

For a mesh with variable :math:`\epsilon`, the left hand side of Poisson’s equation is more complex:

.. math:: \nabla\cdot(\epsilon\nabla\phi)=-\rho

In one (radial) dimension, the gradient operator is of the same form regardless of coordinate system (Cartesian, cylindrical, spherical):

.. math:: -(E_x)=\nabla\phi\Rightarrow\frac{\mathrm{d}\phi}{\mathrm{d}x}=\frac{\phi_{i+1}-\phi_{i-1}}{2\Delta x}+\mathcal{O}(\Delta x^2)

For one-dimensional planar meshes with uniform permivity :math:`\epsilon_0`, as studied here, the solution to Poisson’s equation yields a tridiagonal matrix where the potential at node :math:`i` may be solved for in terms of nodes :math:`i+1` and :math:`i-1`:

.. math:: \phi_{i-1}-2\phi_{i}+\phi_{i+1}=-\frac{\rho_i(\Delta x)^2}{\epsilon_0}

:math:`\rho_i` is the charge density at node :math:`i` and is obtained from particle data. :math:`\Delta x` is the mesh spacing. Boundary conditions are needed to close the system at the mesh points. In this study, ideal conductors were used (Dirichlet boundary conditions).  ``oopd1`` deals with all three 1D coordinate systems as described above with constant and variable permitivity as well several different types of boundary conditions.

Particle Push
-------------

The coordinates, :math:`x`, and velocities, :math:`v`, of the particles are initially prescribed. If only the force due to the electric field is considered, the non-relativistic equations of motion for particle :math:`j` are:

.. math:: \frac{\mathrm{d}x_j}{\mathrm{d}t}=v_j

.. math:: \frac{\mathrm{d}v_j}{\mathrm{d}t}=\frac{qE(x_j)}{m}


:math:`x_j` and :math:`v_j` are the :math:`x`-coordinate and component
of velocity, respectively. :math:`q` and :math:`m` are the charge and mass of particle :math:`j` (respectively), and :math:`E(x_j)` is the :math:`x`-component of electric field at particle :math:`j`.

Linear interpolation is used to find the electric field at particle :math:`j` from the nodal values:

.. math:: E(x_j)=E_{i+1}\cdot\frac{x_j-x_i}{x_{i+1}-x_i}+E_i\cdot\left (1-\frac{x_j-x_i}{x_{i+1}-x_i} \right )

The particle is assumed to be located between node :math:`i` and :math:`i+1` in this equation.

A discrete scheme used in PIC to advance the equations of motion is as follows:

.. math:: v_{new}=v_{old}+\frac{qE}{m}\cdot\Delta t

.. math:: x_{new}=x_{old}+v_{new}\cdot\Delta t

:math:`\Delta t` is the time step used. If the position and velocity components are known at the same time, this is (forward) Euler’s method (1st order accurate). If the velocity is staggered half a time step with respect to position, this is the “leap-frog” (midpoint rule) method (2nd order accurate). The latter is acoomplished by initially advancing the velocity of all particles half a time step backwards using the initial field.

Once the new position of particle :math:`j` is known, its charge is weighed to the mesh using the inverse of the formula used to gather the electric field to the particle. The resulting charge density is then
used in the Poisson solve.

Accuracy Requirements for PIC
-----------------------------

The basic discretization parameters for (explicit) PIC are mesh spacing, time step, and the number of particles per cell. For electrostatic PIC, the mesh spacing, :math:`\Delta x`, and time step, :math:`\Delta t`, must be determined from Debye length, :math:`\lambda_D`, and the plasma frequency, :math:`\omega_P`, respectively, for accuracy and stability:

.. math:: \Delta x < \lambda_D=\left ( \frac{\epsilon_0 T}{en} \right )^{1/2}

.. math:: (\Delta t)^{-1}\gg\omega_P=\left ( \frac{e^2n}{\epsilon_0 m} \right )^{1/2}


In these equations, :math:`T` is the plasma temperature in volts, :math:`n` is the plasma density, :math:`\epsilon_0` is the permitivity of
free space, :math:`e` is the elementary charge, and :math:`m` is the mass of the lightest species.

Violation of these conditions will result in inaccurate and possibly unstable solutions. Typically, :math:`\Delta x\approx 0.5\lambda_D` and :math:`\Delta t\approx 0.1\omega_P^{-1}`.

Building
~~~~~~~~

MPI C++ compiler is required to build and run SIMPIC.

.. code-block:: bash

  git clone git@bitbucket.org:kosl/elmfireng.git
  cd elmfireng
  git fetch && git checkout feature/simpic
  cd simpic
  make
  make html
  xdg-open build/html/index.html

Running with MPI
~~~~~~~~~~~~~~~~

.. code-block:: bash

  ./runsimpic.sh 2> run.log
  mpirun -np 4 a.out -ppc 100 -ncpp 64 -nt 200 -dtfactor 0.001 -lhsv 25000
  rank 0 of 4 processors
  MPI version: 3.1, MPI_Wtime precision: 1e-09
  t=1.12e-09, dt=5.6e-12, ntimesteps=200, density=1e+13, wp=1.79e+08,
  np2c=3.91+08 6400 particles, ng=65, nc=64, L=1, Ll=0.25, dx=0.00390625,
  area=1

  Timings:
  Particles time: 0.111255 [5.0%]
  Fields time: 0.000620347 [0.0%]
  MPI time: 0.154564 [6.9%]
  Diagnostics time: 1.93947 [86.9%]
  Initialization time: 0.00460355 [0.2%]

  Total time: 2.23218 (2.21051 [99.0%])
  569640 (1.14291e+07) particles/second

Diagnostics
~~~~~~~~~~~

Change to `diagnosticsflag = true` in :file:`simpic.cpp` to get
diagnostic :file:`.dat` files of:

  - density
  - E electric field
  - phi potential
  - vxx (phase space)
  - nt is time evolution of number of particles per processor

File format for each processor is::

  time relative_position value

Defining `DEBUG` symbol in :file:`global.h` produces additional
prints to `stderr` stream.

.. note::

   Timings are not representative when defining `DEBUG` and
   `diagnosticsflag = true`.


Variables
~~~~~~~~~

.. code-block:: bash

 NT=200  # of time steps
 PPC=100 # of particles per cell
 CPP=64  # of cell per process
 DTFACTOR=0.001 #defines a fraction of dt vs. time steps from plasma frequency;
		#must be positive
 NPROC=4 # of processors
 LHSV=25000 #applied voltage on left-hand side; RHS is grounded;

`density = 1.E13`
  density of electrons (:math:`n`)
`epsilon = 8.85E-12`
  permitivity of free space (:math:`\epsilon_0`)
`nc`
  number of cells per processor (CPP=64)
`ng`
  number of gridpoints is always one more than number of cells. ng=nc+1
`npart`
  number of (super)particles per processor
  = number of cells * number of particles per cell
`L`
  total length of the system
`q`, `m`
  elementary charge and electron mass
`xl, xr`
  *x* position of left and right bounaries for each processor
`Ll`
  length of the system for each processor
`dx`
  length of each cell :math:`dx=Ll/nc`,
  where `nc` is number of cells per processor
`nl`
  rank of the processor on the left.
  Process 0 has last process on the left in a cyclic manner.
`nr`
  rank of the processor on the right.
  Last process has process 0 on the right in cyclic manner.
`nlp`
  number of `lhsbuf` values to be communicated to the left processor.
`nrp`
  number of `rhsbuf` values to be communicated to the right processor.
`np2c`
  number of (electron) particles per cell
  = density * volume of each processor covers/number of particles per processor
`wp`
  electron plasma frequency :math:`\omega_p^2 = \frac{n q^2}{\epsilon_0 m}`
`dtfactor=0.001`
  fraction of plasma frequency time :math:`\tau = 1/\omega_p` to be used as
`dt`
  time step :math:`\delta t=dtfactor\cdot\tau`
`pdata[2*nproc*npart]`
  phase-space data array of particle *x* positions and *v* velocities
  alternated. Index `j` is *x* position and `j+1` is velocity.
`lhsbuf[2*nproc*npart], rhsbuf[2*nproc*npart]`
  buffers of particle positions and velocities on the left and right.
`narray[ng]`
  density array
`phiarray[ng]`
  potential array
`Earray[ng]`
  Electric field array
`nback[ng]`
  not used. intert. backup.
`scale`
  generic scaling of differences equation that usually multiplies
  with :math:`1/\Delta x` or similar weighting.

Main loop
~~~~~~~~~

moveparticles(dt)
     Computes from the electric field *E* new velocity
     for each particle and moves it to new position. If the particles
     moves out of processor bounds [xl, xr] then it is moved to left
     or right hand side buffer for communication to neighboring
     processors.
communicateparticles()
     Each processor sends particles to the left and right. Before
     communition of arrays starts, sizes are exchanged.
advancefields(dt)
     Tridiagonal matrix of Poisson equation
     :math:`\phi_{j+1}-2\phi_j+\phi_{j-1}=p_j`
     is solved with Gaussian elimination  [BiLa85]_ by each processor
     and then fields are communicated to all processors. Then Laplace
     equation is summed to
     get the potential :math:`phi`-array. From :math:`\phi` negative gradient
     gives E-array.

CPU StarPU
==========

Converting MPI version of SimPIC to task based CPU version follows
StarPU guidelines for MPI support.

Integrating StarPU in a Make Build System
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We begin with adding StarPU MPI header to :file:`simpic.cpp`

.. code-block:: c++

    #include <starpu_mpi.h>

and modifying :file:`Makefile` for compiler and linking flags

.. code-block:: make

   CFLAGS += $$(pkg-config --cflags starpumpi-1.3)
   LDLIBS += $$(pkg-config --libs starpumpi-1.3)

   debug:
	mpic++ -g $(CFLAGS) simpic.cpp message.cpp $(LDLIBS)

Not using the StarPU MPI support
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For a start we can concentrate on the StarPU task breakdown, leaving
original MPI communication intact. StarPU data management with
Distributed Shared Memory (DSM) needs to be provided in a consistent
manner through the MSI protocol (Modified / Shared / Invalid).

Examining ``moveparticles(Scalar dt)`` as our first codelet the data
that is send over MPI in ``communicateparticles()`` is basically
received into complete ``pdata`` phase-space vector from buffers
``lhsbuf`` and ``rhsbuf`` that are practically sending smaller number
of particles to the lhs and rhs. Number of particles that left
computational domain ``[xl, xr]`` of each processor is counted by
``nlp`` and `nrp`. Particles move according to *E* field defined at
the grid points. Each processor is using ``ng`` grid points in
``Earray`` vector.

Parameters that do not change in communication within the codelet are
left ``nl`` and right ``nr`` neighboring processor numbers, domain
boundary ``[xl, xr]`` pair, time step size ``dt``, ``ng`` grid points,
and factors such as charge-mass ``qm`` and cell-size ``dx`` that are
pre-computed for faster computation in subroutines. All these trivial
parameters that are not handled by StarPU data management can be
passed to the codelet in one structure as a pointer to the structure
containing all codelet arguments.

.. highlight:: c++

So, instead of global variables ::

   int nl, nr;

we encapsulate them into ::

   struct cl_args_t {
     int nl, nr;
   };

and create an instance of the struct ::

   cl_args_t cl_args;

and replace ``nl`` with ``cl_args.nl`` and ``nr`` with ``cl_args.nr``
respectively everywhere in the code in order to compile. We gradually
add more parameters in step by step manner. For example we move ``dt``
into ::

   struct cl_args_t {
     Scalar dt;
     int nl, nr;
   };

In the end all the necessary parameters are put into the struct ::

   struct cl_args_t {
     Scalar qm;
     Scalar dt;
     Scalar dx;
     Scalar xl;
     Scalar xr;
     int nl, nr;
     int nlp, nrp;
   };

Besides replacing, i.e. ``nl`` to ``cl_args.nl``, we must also add the struct
as parameter, since it will not be global anymore.

Now we focus on function ``void moveparticles``. The initial signature takes
``Scalar dt`` as argument, but now we will change it to
``struct cl_args_t cl_args``

.. code-block:: diff

   - void moveparticles(Scalar dt)
   + void moveparticles(struct cl_args_t *cl_args)
     {
   +   struct cl_args_t *cl_args = (struct cl_args_t *) _cl_args;

The calling of this function inside ``mainloop(Scalar tmax, Scalar dt)`` is
changed

.. code-block:: diff

   - moveparticles(dt)
   + moveparticles(&cl_args)

Now, since the pointer is passed to the function, we need to change the access
to the variables inside the struct

.. code-block:: diff

   - cl_args.qm
   + cl_args->qm

.. note::

   The refactoring of ``cl_args`` should be complete in a way that it can
   be removed from global scope. For a start, when using CPU only codelet,
   all ``cl_args`` will be read-only and will not cause locking problems
   and transfers from main memory. When GPU codelet is introduced then
   ``cl_args``` needs to removed from global scope and pass its parameters
   to subroutines from the codelet.

We can now start creating StarPU task for the function ``movepartparticles``.
First we need a struct *starpu_codelet* that contains information on what
function to be called and if there are any buffers to send.

.. code-block:: cpp

   static struct starpu_codelet cl =
     {
       .cpu_funcs = { moveparticles },
       .cpu_funcs_name = { "moveparticles" },
       .nbuffers = 3, // Number of buffers (arrays) the function needs
       .modes = {STARPU_RW, STARPU_RW, STARPU_RW} // Read/Write modes for buffers
     }

Now we can create a task

In ``global.h``  we define a global task

.. code-block:: cpp

   struct starpu_task *task;

In ``simpic.cpp``:

.. code-block:: cpp

   task = starpu_tash_create();
   task->cl = &cl;
   task->handles[0] = pdata_handle;
   task->handles[1] = lhsbuf_handle;
   task->handles[2] = rhsbuf_handle;
   task->cl_arg = &cl_args; // Struct containing constants
   task->cl_arg_size = sizeof(cl_args_t);
   task->synchronous = 1; // Blocking task
   task->destroy = 0; // Do not destroy the task after submitting because it is a repeatable one.

Before we submit the task we have to fix the signature of ``moveparticles``

.. code-block:: diff

   - void moveparticles(struct cl_args_t *cl_args)
   +
   + void moveparticles(void *buffers[], void * _cl_args)
     {
      int i, j;
      Scalar E, x, v;
      struct cl_args_t *cl_args = (struct cl_args_t *) _cl_args;

   +  Scalar *pdata = (Scalar *) STARPU_VECTOR_GET_PTR(buffers[0]);
   +  Scalar *lhsbuf = (Scalar *) STARPU_VECTOR_GET_PTR(buffers[1]);
   +  Scalar *rhsbuf = (Scalar *) STARPU_VECTOR_GET_PTR(buffers[2]);


The argument signature ``void *buffers[], void * _cl_args`` is default for all
StarPU CPU task functions. To get a vector buffer you call
``STARPU_VECTOR_GET_PTR`` on the input argument buffers[n], where n is is the
chosen before hand when defining a StarPU task.

Now we can submit the task inside the function ``mainloop``

.. code-block:: diff

   - moveparticles(&cl_args);
   + starpu_task_submit(task);


StarPU notes
============

Building on taito-gpu cluster
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: bash

   wget http://starpu.gforge.inria.fr/testing/starpu-1.3/starpu-nightly-latest.tar.gz
   tar xvf starpu-nightly-latest.tar.gz
   cd starpu-1.3.1.99/
   mkdir build
   cd build
   ../configure --prefix=$HOME/starpu --without-hwloc

Building on Bison GPU node
~~~~~~~~~~~~~~~~~~~~~~~~~~


.. code-block:: bash

    wget http://starpu.gforge.inria.fr/testing/starpu-1.3/starpu-nightly-latest.tar.gz

    # Other releases are available on http://starpu.gforge.inria.fr/files/
    # Stable release can be used instead:
    # wget http://starpu.gforge.inria.fr/files/starpu-1.3.3/starpu-1.3.3.tar.gz

    tar xvf starpu-nightly-latest.tar.gz
    mkdir starpu-1.3.3.99/build

    cd starpu-1.3.3.99/build
    module load cuda-10.1.243-gcc-8.3.0-qpdenfa
    ../configure --prefix=${HOME}/starpu --enable-openmp --enable-mpi-check
    make -j 8
    make install

Building on D.A.V.I.D.E. cluster
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On [DAVIDE]_ cluster the following build procedure is recommended.
One needs to configure [StarPU]_ on compute node and then compile the
library on the login node due to missing *Mellanox Hierarchical
Collectives* (hcoll) and *Mellanox Messaging* (mxm) libraries on
compute nodes.

.. code-block:: bash

    wget http://starpu.gforge.inria.fr/testing/starpu-1.3/starpu-nightly-latest.tar.gz
    tar xvf starpu-nightly-latest.tar.gz
    mkdir starpu-1.3.2.99/build

    cd starpu-1.3.2.99/build
    module load cuda/9.2.88 gnu/6.4.0 openmpi/3.1.0--gnu--6.4.0
    module load szip/2.1.1--gnu--6.4.0 hdf5/1.10.4--openmpi--3.1.0--gnu--6.4.0

    export OMP_NUM_THREADS=8
    srun -N1 --time=03:00:00 --ntasks-per-node=16 --gres=gpu:4 -A FUAD3_SOLDyn3G -p dvd_fua_prod --pty bash

    nvidia-smi topo -m
    ../configure --prefix=${HOME}/starpu --enable-maxcpus=512 --enable-maxcudadev=128 --enable-maxopencldev=128 --enable-maxnodes=32 --enable-maxbuffers=32 --with-hdf5-include-dir=${HDF5_INC} --with-hdf5-lib-dir=${HDF5_LIB} --enable-openmp
    exit

    make -j16
    make install

When configuring is done the following important configuration options
are printed and ready for make and installation.

.. code-block:: bash

   configure:

        CPUs   enabled: yes
        CUDA   enabled: yes
        OpenCL enabled: yes
        MIC    enabled: no

        Compile-time limits
        (change these with --enable-maxcpus, --enable-maxcudadev,
        --enable-maxopencldev, --enable-maxmicdev, --enable-maxnodes,
        --enable-maxbuffers)
        (Note these numbers do not represent the number of detected
        devices, but the maximum number of devices StarPU can manage)

        Maximum number of CPUs:                     512
        Maximum number of CUDA devices:             128
        Maximum number of OpenCL devices:           128
        Maximum number of MIC threads:              0
        Maximum number of MPI master-slave devices: 1
        Maximum number of memory nodes:             32
        Maximum number of task buffers:             32

        GPU-GPU transfers: yes
        Allocation cache:  yes

        Magma enabled:     no
        BLAS library:      none
        hwloc:             yes
        FxT trace enabled: no

        Documentation:     no
        Examples:          yes

        StarPU Extensions:
               StarPU MPI enabled:                            yes
               StarPU MPI(nmad) enabled:                      no
               MPI test suite:                                no
               Master-Slave MPI enabled:                      no
               FFT Support:                                   yes
               Resource Management enable:                    no
               OpenMP runtime support enabled:                yes
               Cluster support enabled:                       no
               SOCL enabled:                                  yes
               SOCL test suite:                               no
               Scheduler Hypervisor:                          no
               simgrid enabled:                               no
               ayudame enabled:                               no
               Native fortran support:                        yes
               Native MPI fortran support:                    no
               Support for multiple linear regression models: yes

Verifying StarPU installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To test some examples on *compute node* use:

.. code-block:: bash

   srun -N1 --ntasks-per-node=16 --time=03:00:00 --gres=gpu:4 \
     -A FUAD3_SOLDyn3G -p dvd_fua_prod --pty bash
   export LD_LIBRARY_PATH=${HOME}/starpu/lib:${LD_LIBRARY_PATH}
   cd ${HOME}/starpu/lib//starpu/examples/

   ./add_vectors
   [starpu][check_bus_config_file] No performance model for the bus, calibrating...
   [starpu][benchmark_all_gpu_devices] CUDA 0...
   [starpu][benchmark_all_gpu_devices] CUDA 1...
   [starpu][benchmark_all_gpu_devices] CUDA 2...
   [starpu][benchmark_all_gpu_devices] CUDA 3...
   [starpu][benchmark_all_gpu_devices] CUDA 0 -> 1...
   [starpu][measure_bandwidth_between_dev_and_dev_cuda] GPU-Direct 1 -> 0
   [starpu][measure_bandwidth_between_dev_and_dev_cuda] GPU-Direct 0 -> 1
   [starpu][benchmark_all_gpu_devices] CUDA 0 -> 2...
   [starpu][benchmark_all_gpu_devices] CUDA 0 -> 3...
   [starpu][benchmark_all_gpu_devices] CUDA 1 -> 0...
   [starpu][measure_bandwidth_between_dev_and_dev_cuda] GPU-Direct 0 -> 1
   [starpu][measure_bandwidth_between_dev_and_dev_cuda] GPU-Direct 1 -> 0
   [starpu][benchmark_all_gpu_devices] CUDA 1 -> 2...
   [starpu][benchmark_all_gpu_devices] CUDA 1 -> 3...
   [starpu][benchmark_all_gpu_devices] CUDA 2 -> 0...
   [starpu][benchmark_all_gpu_devices] CUDA 2 -> 1...
   [starpu][benchmark_all_gpu_devices] CUDA 2 -> 3...
   [starpu][measure_bandwidth_between_dev_and_dev_cuda] GPU-Direct 3 -> 2
   [starpu][measure_bandwidth_between_dev_and_dev_cuda] GPU-Direct 2 -> 3
   [starpu][benchmark_all_gpu_devices] CUDA 3 -> 0...
   [starpu][benchmark_all_gpu_devices] CUDA 3 -> 1...
   [starpu][benchmark_all_gpu_devices] CUDA 3 -> 2...
   [starpu][measure_bandwidth_between_dev_and_dev_cuda] GPU-Direct 2 -> 3
   [starpu][measure_bandwidth_between_dev_and_dev_cuda] GPU-Direct 3 -> 2
   [starpu][benchmark_all_gpu_devices] OpenCL 0...
   [starpu][benchmark_all_gpu_devices] OpenCL 1...
   [starpu][benchmark_all_gpu_devices] OpenCL 2...
   [starpu][benchmark_all_gpu_devices] OpenCL 3...
   [starpu][check_bus_config_file] ... done
   [starpu][initialize_lws_policy] Warning: you are running the default
   lws scheduler, which is not a very smart scheduler, while the
   system has GPUs or several memory nodes. Make sure to read the
   StarPU documentation about adding performance models in order to be
   able to use the dmda or dmdas scheduler instead.

   [lkos0000@davide44 examples]$ ./add_vectors
   [starpu][initialize_lws_policy] Warning: you are running the
   default lws scheduler, which is not a very smart scheduler, while
   the system has GPUs or several memory nodes. Make sure to read the
   StarPU documentation about adding performance models in order to be
   able to use the dmda or dmdas scheduler instead.

    [lkos0000@davide44 examples]$ ./hello_world
    [starpu][initialize_lws_policy] Warning: you are running the
    default lws scheduler, which is not a very smart scheduler, while
    the system has GPUs or several memory nodes. Make sure to read the
    StarPU documentation about adding performance models in order to
    be able to use the dmda or dmdas scheduler instead.
    Hello world
    (params = {1, 2.000000} )
    Callback function got argument 0x42

Building OpenMP example
~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: bash

   cd examples/openmp
   export PKG_CONFIG_PATH=${HOME}/starpu/lib/pkgconfig
   gcc -fopenmp -O2 -g vector_scal_omp.c -o vector_scal_omp $(pkg-config --cflags starpu-1.0) $(pkg-config --libs starpu-1.0)
   export LD_LIBRARY_PATH=${HOME}/starpu/lib:${LD_LIBRARY_PATH}
   ./vector_scal_omp



.. only:: html


   .. rubric:: Rerefences

.. [BiLa85] C.K. Birdsall, A.B Langdon,
            *Plasma Physics via Computer Simulation*, Adam Hilger press,
	    1991, p.446

.. [OOPD1]  https://ptsg.egr.msu.edu/#Software

.. [StarPU] Official webpage: http://starpu.gforge.inria.fr/

            Documentation: http://starpu.gforge.inria.fr/doc/html/

.. [DAVIDE] https://wiki.u-gov.it/confluence/display/SCAIUS/UG3.2%3A+D.A.V.I.D.E.+UserGuide


